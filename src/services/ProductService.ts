export interface Product {
    id: number;
    name: string;
    price: number;
    minCount: number;
    images: string[];
}

export default class ProductService {
    getProducts(): Product[] {
        return JSON.parse(localStorage.products || '{}');
    }
    getProductById(id: number) {
        const allProducts: Product[] = this.getProducts();
        return allProducts.filter(product => product.id == id)[0];
    }
}
