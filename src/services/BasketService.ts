export default class BasketService {
    getItems() {
        return JSON.parse(localStorage.basket || '{}');
    }

    removeItems(id: number): void {
        const items = this.getItems();
        delete items[id];
        localStorage.basket = JSON.stringify(items);
    }

    updateItems(id: number, count: number): void {
        const allItems = this.getItems();
        allItems[id] = count;
        localStorage.basket = JSON.stringify(allItems);
    }
}
